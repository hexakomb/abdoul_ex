from django.urls import path
from django.conf.urls import url
from .import views


app_name='contacts'
urlpatterns = [
    path(r'<pk>/contacts',views.contact_list,name="list"),
    path('<int:id>/contacts/create',views.contact_create,name="create"),
    # url(r'^promote/<int:id>',views.lead_update,name="promote"),
    path('contacts/update/<int:id>',views.contact_update,name="update"),
    path('contacts/delete/<int:id>',views.contact_delete,name="delete")
    # url(r'^(?P<slug>[\w-]+)/$',views.article_detail,name="detail"),
    
]
