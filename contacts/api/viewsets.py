from contacts.models import Contact
from contacts.api.serializer import ContactSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action


# class StudentViewset(viewsets.ViewSet):
#     def list(self,request):
#         queryset=Student.objects.all()
#         serializer=StudentSerializer(queryset,many=True)
#         return Response(serializer.data)


class ContacttViewset(viewsets.ModelViewSet):
    queryset=Contact.objects.all()
    serializer_class=ContactSerializer

    