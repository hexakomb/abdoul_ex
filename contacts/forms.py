from django import forms
from .import models



class CreateContact(forms.ModelForm):
    class Meta:
        model=models.Contact
        fields=['names','email','address','phone','isprimary']

class ContactForm(forms.Form):
     names=forms.CharField(max_length=100)
     email=forms.EmailField(max_length=100)
     address=forms.CharField(max_length=200)



