from django.shortcuts import render,redirect
from .models import Contact
from leads.models import Lead
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .import forms

# Create your views here.

listurl='contacts:list'

@login_required(login_url="/accounts/login/")
def contact_list(request,pk):
    lead=Lead.objects.get(id=pk)
    contacts = Contact.objects.filter(lead=lead)
    return render(request,'contacts/contact_list.html',{'contacts':contacts})
@login_required(login_url="/accounts/login/")
def contact_create(request,id):
    lead=Lead.objects.get(id=id)
    if request.method =='POST':
        form=forms.CreateContact(request.POST or None)
        if form.is_valid():
            # save articles to db
            instance=form.save(commit=False)
            instance.lead=lead
            instance.save()
            return redirect(listurl,pk=lead.id)
    else:
        form=forms.CreateContact()
    return render(request,'contacts/contact_create.html',{'form':form})
@login_required(login_url="/accounts/login/")
def contact_update(request,id):
    contactinstance=Contact.objects.get(id=id)

    form=forms.CreateContact(request.POST or None,instance=contactinstance)
    if form.is_valid():
        
        form.save()
        return redirect(listurl,pk=contactinstance.lead.id)
    
    return render(request,'contacts/contact_create.html',{'form':form})
@login_required(login_url="/accounts/login/")
def contact_delete(request,id):
    contactinstance=Contact.objects.get(id=id)
    contactinstance.delete()
        
    return redirect(listurl,pk=contactinstance.lead.id)