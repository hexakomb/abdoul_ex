from django.db import models
from leads.models import Lead
# Create your models here.



class Contact(models.Model):
    names=models.CharField(max_length=100)
    email=models.EmailField(max_length=100)
    address=models.CharField(max_length=100)
    phone=models.CharField(max_length=100)
    isprimary = models.BooleanField(default=False)
    lead=models.ForeignKey(Lead,default=None,on_delete=models.CASCADE)


    def __str__(self):
        return self.names