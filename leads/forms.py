from django import forms
from .import models



class CreateLead(forms.ModelForm):
    class Meta:
        model=models.Lead
        fields=['names','description'] 

class LeadForm(forms.Form):
     names=forms.CharField(max_length=100)
     description=forms.CharField(max_length=200)