from django.shortcuts import render,redirect
from .models import Lead,leadType
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .import forms
from leads.api.viewsets import LeadSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
# Create your views here.
createurl='leads/lead_create.html'
list_url='leads:list'

@login_required(login_url="/accounts/login/")
def lead_list(request):
    leads=Lead.objects.all()
    return render(request,'leads/leads_list.html',{'leads':leads})

@login_required(login_url="/accounts/login/")
def lead_detail(request,id):
    # return HttpResponse(slug)
    lead=Lead.objects.get(id=id)
    return render(request,'leads/lead_detail.html',{'lead':lead})

@login_required(login_url="/accounts/login/")
def lead_create(request):
    title='Create'
    buttonText='Save'
    if request.method =='POST':
        form=forms.CreateLead(request.POST,request.FILES)
        if form.is_valid():
            # save articles to db
    
            form.save()
            # instance.save()
            return redirect(list_url)
    else:
        form=forms.CreateLead()
    return render(request,createurl,{'form':form,'title':title,'buttonText':buttonText})
@login_required(login_url="/accounts/login/")
def lead_promote(request,id):
    leadinstance=Lead.objects.get(id=id)
     
    if leadinstance.leadtype == 'lead':
            leadinstance.leadtype=leadType.choices()[1][1]
    elif leadinstance.leadtype == 'potential':
            leadinstance.leadtype=leadType.choices()[2][1]
    elif leadinstance.leadtype == 'opportunity':
            leadinstance.leadtype=leadType.choices()[3][1]
    else:
            leadinstance.leadtype=leadType.choices()[3][1]

    leadinstance.save()
    return redirect(list_url)
    
@login_required(login_url="/accounts/login/")
def lead_update(request,id):
    leadinstance=Lead.objects.get(id=id)
    title='Update'
    buttonText='Update'
    form=forms.CreateLead(request.POST or None,instance=leadinstance)
    if form.is_valid():
        
        form.save()
        return redirect(list_url)
    
    return render(request,'leads/lead_create.html',{'form':form,'title':title,'buttonText':buttonText})
@login_required(login_url="/accounts/login/")
def lead_delete(request,id):
    leadinstance=Lead.objects.get(id=id)
    leadinstance.delete()
       
    return render(request,'leads:list')

@api_view(['GET','POST'])
def leadListview(request):
    if request.method == 'GET':
        leadall = Lead.objects.all()
        serializer = LeadSerializer(leadall, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = LeadSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

