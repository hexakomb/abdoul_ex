from leads.models import Lead,leadType
from leads.api.serializer import LeadSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action,api_view
from rest_framework import status

# class StudentViewset(viewsets.ViewSet):
#     def list(self,request):
#         queryset=Student.objects.all()
#         serializer=StudentSerializer(queryset,many=True)
#         return Response(serializer.data)


class LeadtViewset(viewsets.ModelViewSet):
    queryset=Lead.objects.all()
    serializer_class=LeadSerializer

