from django.db import models
# Create your models here.
from enum import Enum

class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((i.value, i.name) for i in cls)

class leadType(ChoiceEnum):
    lead=1
    potential=2
    opportunity=3
    customer=4

class Lead(models.Model):
    names=models.CharField(max_length=100)
    leadtype=models.CharField(max_length=100, choices=leadType.choices(),default=leadType.choices()[0][1])
    description=models.TextField()
    def __str__(self):
        return self.names