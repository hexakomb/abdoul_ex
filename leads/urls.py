from django.urls import path
from django.conf.urls import url
from .import views


app_name='leads'
urlpatterns = [
    url(r'^$',views.lead_list,name="list"),
    url(r'^create/$',views.lead_create,name="create"),
    # url(r'^promote/<int:id>',views.lead_update,name="promote"),
    path('promote/<int:id>',views.lead_promote,name="promote"),
    path('<int:id>/detail',views.lead_detail,name="promote"),
    path('update/<int:id>',views.lead_update,name="update"),
    path('delete/<int:id>',views.lead_delete,name="delete"),
    path('api/leadlist',views.leadListview,name="apitest")
    # url(r'^(?P<slug>[\w-]+)/$',views.article_detail,name="detail"),
    
]
