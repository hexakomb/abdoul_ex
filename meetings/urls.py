from django.urls import path
from django.conf.urls import url
from .import views


app_name='meetings'
urlpatterns = [
    path(r'<pk>/meetings',views.meeting_list,name="list"),
    path('<int:id>/meetings/create',views.meeting_create,name="create"),
    # url(r'^promote/<int:id>',views.lead_update,name="promote"),
    path('meetings/update/<int:id>',views.meeting_update,name="update"),
    path('meetings/delete/<int:id>',views.meeting_delete,name="delete")
    # url(r'^(?P<slug>[\w-]+)/$',views.article_detail,name="detail"),
    
]
