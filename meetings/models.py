from django.db import models
from leads.models import Lead
# Create your models here.



class Meeting(models.Model):
    title=models.CharField(max_length=100)
    venue=models.CharField(max_length=100)
    about=models.CharField(max_length=100)
    time=models.DateTimeField()
    conclusion=models.TextField()
    meetingminute=models.FileField()
    lead=models.ForeignKey(Lead,default=None,on_delete=models.CASCADE)


    def __str__(self):
        return self.title