from django import forms
from .import models



class CreateMeeting(forms.ModelForm):
    class Meta:
        model=models.Meeting
        fields=['title','venue','about','time','conclusion','meetingminute'] 



class MeetingForm(forms.Form):
     title=forms.CharField(max_length=100)
     venue=forms.CharField(max_length=100)
     about=forms.CharField(max_length=200)