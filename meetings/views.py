from django.shortcuts import render,redirect
from .models import Meeting
from leads.models import Lead
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .import forms

# Create your views here.

listurl='meetings:list'

@login_required(login_url="/accounts/login/")
def meeting_list(request,pk):
    lead=Lead.objects.get(id=pk)
    meetings = Meeting.objects.filter(lead=lead)
    return render(request,'meetings/meeting_list.html',{'meetings':meetings,'lead':lead})

@login_required(login_url="/accounts/login/")
def meeting_create(request,id):
    lead=Lead.objects.get(id=id)
    if request.method =='POST':
        form=forms.CreateMeeting(request.POST or None,request.FILES)
        if form.is_valid():
            # save articles to db
            instance=form.save(commit=False)
            instance.lead=lead
            instance.save()
            return redirect(listurl,pk=lead.id)
    else:
        form=forms.CreateMeeting()
    return render(request,'meetings/meeting_create.html',{'form':form})

@login_required(login_url="/accounts/login/")
def meeting_update(request,id):
    meetinginstance=Meeting.objects.get(id=id)

    form=forms.CreateMeeting(request.POST or None,instance=meetinginstance)
    if form.is_valid():
        
        form.save()
        return redirect(listurl,pk=meetinginstance.lead.id)
    
    return render(request,'meetings/meeting_create.html',{'form':form})

@login_required(login_url="/accounts/login/")
def meeting_delete(request,id):
    meetinginstance=Meeting.objects.get(id=id)
    meetinginstance.delete()
        
    return redirect(listurl,pk=meetinginstance.lead.id)