from leads.api.viewsets import LeadtViewset
from contacts.api.viewsets import ContacttViewset
from rest_framework import routers


router=routers.DefaultRouter()
router.register('leads',LeadtViewset,basename='lead')
router.register('contacts',ContacttViewset,basename='contact')