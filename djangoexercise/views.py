from django.http import HttpResponse
from django.shortcuts import render
from formtools.wizard.views import SessionWizardView
from contacts.forms import ContactForm
from leads.forms import LeadForm
from meetings.forms import MeetingForm
from django.core.files.storage import FileSystemStorage
from django.core.files.storage import DefaultStorage
from django.conf import settings
import os

def home_page(request):
       # return HttpResponse('Home Page')
   return render(request,'Homepage.html')

FORMS_REG = [("reg_step1", ContactForm),
         ("reg_step2", LeadForm),
         ("reg_step3", MeetingForm),
]

TEMPLATES_REG = {"reg_step1": "reg_step1.html",
             "reg_step2":"reg_step2.html",
              "reg_step3":"reg_step3.html"
            
             }


class CustomerWizard(SessionWizardView):
    template_name="customer_form.html"

    # file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'media'))
    file_storage = DefaultStorage()


    def done(self,form_list,**kwargs):
            form_data=process_form_data(form_list)
    
            return render(self.request,'done.html',{
            'form_data': [form.cleaned_data for form in form_list],
        })

def process_form_data(form_list):
    form_data=[form.cleaned_data for form in form_list]
    print(form_data[0]['names'])
    print(form_data[0]['email'])
    print(form_data[0]['address'])
    print(form_data[1]['names'])
    print(form_data[1]['description'])

    print(form_data[2]['title'])
    print(form_data[2]['venue'])

    return form_data


class CustomerWizard1(SessionWizardView):
    def get_template_names(self):
        return [TEMPLATES_REG[self.steps.current]]
    def done(self,form_list,**kwargs):
            form_data=process_form_data(form_list)
    
            return render(self.request,'done.html',{
            'form_data': [form.cleaned_data for form in form_list],
        })