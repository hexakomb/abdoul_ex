"""djangoexercise URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .import views as view
from .router import router
from rest_framework.authtoken import views
from contacts.forms import ContactForm,CreateContact
from leads.forms import LeadForm,CreateLead
from meetings.forms import MeetingForm,CreateMeeting

baseurl=r'^leads/'
urlpatterns = [
    path('admin/', admin.site.urls),
     url(r'^leads/',include('leads.urls')),
     url(baseurl,include('contacts.urls')),
      url(baseurl,include('meetings.urls')),
      url(r'^accounts/',include('acccounts.urls')),
      url(r'^$',view.home_page,name="home"),
      path('api/',include(router.urls)),
      path('api-token-auth/',views.obtain_auth_token,name='api-token-auth'),
      path('customers',view.CustomerWizard.as_view([CreateContact,CreateLead,CreateMeeting])),
      path('customers/all',view.CustomerWizard1.as_view(view.FORMS_REG))
]


urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)